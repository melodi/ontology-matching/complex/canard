
#echo "********** Source: AGROVOC - Target: AgronomicTaxon - Query: TAXON ***********"
#java -jar ComplexMatcher.jar http://localhost:3030/AgroVoc/ http://localhost:3030/AgronomicTaxon/ needs/taxon-agrovoc.sparql query_templates/AgroVoc/ query_templates/generic/ > output/taxon/taxon-agrovoc-agrotaxon.txt
#echo "************* Source: AGROVOC - Target: DBpedia- Query: TAXON ****************"
#java -jar ComplexMatcher.jar http://localhost:3030/AgroVoc/ http://dbpedia.org/ needs/taxon-agrovoc.sparql query_templates/AgroVoc/ query_templates/DBpedia/ > output/taxon/taxon-agrovoc-dbpedia.txt
#echo "************** Source: AGROVOC - Target: TaxRef - Query: TAXON ***************"
#java -jar ComplexMatcher.jar http://localhost:3030/AgroVoc/ http://erebe-vm2.i3s.unice.fr:8890/ needs/taxon-agrovoc.sparql query_templates/AgroVoc/ query_templates/TaxRef/ > output/taxon/taxon-agrovoc-taxref.txt


#echo "********** Source: AGROTAXON- Target: Agrovoc - Query: TAXON ***********"
#java -jar ComplexMatcher.jar http://localhost:3030/AgronomicTaxon/ http://localhost:3030/AgroVoc/ needs/taxon-agrotaxon.sparql query_templates/generic/ query_templates/AgroVoc/> output/taxon/taxon-agrotaxon-agrovoc.txt
#echo "************* Source: AGROTAXON - Target: DBpedia- Query: TAXON ****************"
#java -jar ComplexMatcher.jar http://localhost:3030/AgronomicTaxon/ http://dbpedia.org/ needs/taxon-agrotaxon.sparql query_templates/generic/ query_templates/DBpedia/ > output/taxon/taxon-agrotaxon-dbpedia.txt
#echo "************** Source: AGROTAXON - Target: TaxRef - Query: TAXON ***************"
#java -jar ComplexMatcher.jar http://localhost:3030/AgronomicTaxon/ http://erebe-vm2.i3s.unice.fr:8890/ needs/taxon-agrotaxon.sparql query_templates/generic/ query_templates/TaxRef/> output/taxon/taxon-agrotaxon-taxref.txt

#echo "********** Source: DBPEDIA- Target: Agrovoc - Query: TAXON ***********"
#java -jar ComplexMatcher.jar http://dbpedia.org/ http://localhost:3030/AgroVoc/ needs/taxon-dbpedia.sparql query_templates/DBpedia/ query_templates/AgroVoc/ > output/taxon/taxon-dbpedia-agrovoc.txt
#echo "************* Source: DBPEDIA - Target: AgronomicTaxon- Query: TAXON ****************"
#java -jar ComplexMatcher.jar http://dbpedia.org/ http://localhost:3030/AgronomicTaxon/ needs/taxon-dbpedia.sparql query_templates/DBpedia/ query_templates/generic/ > output/taxon/taxon-dbpedia-agrotaxon.txt
#echo "************** Source: DBPEDIA - Target: TaxRef - Query: TAXON ***************"
#java -jar ComplexMatcher.jar http://dbpedia.org/ http://erebe-vm2.i3s.unice.fr:8890/ needs/taxon-dbpedia.sparql query_templates/DBpedia/ query_templates/TaxRef/ > output/taxon/taxon-dbpedia-taxref.txt

echo "********** Source: TAXREF- Target: Agrovoc - Query: TAXON ***********"
java -jar ComplexMatcher.jar http://erebe-vm2.i3s.unice.fr:8890/ http://localhost:3030/AgroVoc/ needs/taxon-taxref.sparql query_templates/TaxRef/ query_templates/AgroVoc/ > output/taxon/taxon-taxref-agrovoc.txt
echo "************* Source: TAXREF - Target: AgronomicTaxon- Query: TAXON ****************"
java -jar ComplexMatcher.jar http://erebe-vm2.i3s.unice.fr:8890/ http://localhost:3030/AgronomicTaxon/ needs/taxon-taxref.sparql query_templates/TaxRef/ query_templates/generic/ > output/taxon/taxon-taxref-agrotaxon.txt
echo "************** Source: TAXREF - Target: Dbpedia - Query: TAXON ***************"
java -jar ComplexMatcher.jar http://erebe-vm2.i3s.unice.fr:8890/ http://dbpedia.org/ needs/taxon-taxref.sparql query_templates/TaxRef/ query_templates/DBpedia/ > output/taxon/taxon-taxref-dbpedia.txt
