#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jun 24 15:04:23 2019

@author: thieblin
"""
import re

#onto=["cmt","conference","confOf","edas","ekaw"]
onto=["agrotaxon","agrovoc","dbpedia","taxref"]
#tests=["0.0","0.1","0.2","0.3","0.4","0.5","0.6","0.7","0.8","0.9","1.0"]
#tests=["1","2","3","4","5","6","7","8","9","20","100"]
#tests=["cqa_reassess","sim","query","query_reassess"]
#tests=["Ritze_2010","Faria_2018","ra1","Ontology_merging","Query_rewriting"]
tests=["1","10"]

resToTfile = open("./count/count_taxon.csv","w")
resToTfile.write("pair,ss,sc,cs,cc,tot\n")


for test in tests:
    resfile = open("./count/count_{}.csv".format(test),"w")
    resfile.write("pair,ss,sc,cs,cc,tot\n")
    nbSST=0
    nbSCT=0
    nbCST=0
    nbCCT=0
    nbTT=0
    
    
    for o1 in onto:
        for o2 in onto:
            if(o1!=o2):
                #file = open("./conference/test_{}/{}-{}.edoal".format(test,o1,o2), "r")
                #file=open("/home/thieblin/eclipse-workspace/Conference-dataset-population/alignments/{}/{}-{}.edoal".format(test,o1,o2,"r"))
                file = open("./taxon_local_{}/{}-{}.edoal".format(test,o1,o2), "r")
                content=file.read()
                content=content.replace('\n', '')
                content=content.replace(' ', '')
                content=content.replace('\t', '')
                
                nbTot=content.count("<map>")
                nbSSCla=len(re.findall("<entity1><edoal:Classrdf:about=[^>]+/></entity1><entity2><edoal:Classrdf:about=[^>]+/></entity2>", content))
                nbSSRel=len(re.findall("<entity1><edoal:Relationrdf:about=[^>]+/></entity1><entity2><edoal:Relationrdf:about=[^>]+/></entity2>",content))
                nbSSProp=len(re.findall("<entity1><edoal:Propertyrdf:about=[^>]+/></entity1><entity2><edoal:Propertyrdf:about=[^>]+/></entity2>",content))
                
                nbSXCla=len(re.findall("<entity1><edoal:Classrdf:about=[^>]+/></entity1>", content))
                nbSXRel=len(re.findall("<entity1><edoal:Relationrdf:about=[^>]+/></entity1>",content))
                nbSXProp=len(re.findall("<entity1><edoal:Propertyrdf:about=[^>]+/></entity1>",content))
            

    
                nbXSCla=len(re.findall("<entity2><edoal:Classrdf:about=[^>]+/></entity2>", content))
                nbXSRel=len(re.findall("<entity2><edoal:Relationrdf:about=[^>]+/></entity2>",content))
                nbXSProp=len(re.findall("<entity2><edoal:Propertyrdf:about=[^>]+/></entity2>",content))
                
                nbSS=nbSSCla+nbSSRel +nbSSProp
                nbSC=nbSXCla+nbSXRel+nbSXProp-nbSS
                nbCS=nbXSCla+nbXSRel+nbXSProp-nbSS
                
                nbCC=nbTot-nbSS-nbSC-nbCS
                nbSST+=nbSS
                nbSCT+=nbSC
                nbCST+=nbCS
                nbCCT+=nbCC
                nbTT+=nbTot
                resfile.write("{}-{},{},{},{},{},{}\n".format(o1,o2,nbSS,nbSC,nbCS,nbCC,nbTot))
    
    
    resfile.write("TOTAL,{},{},{},{},{}\n".format(nbSST,nbSCT,nbCST,nbCCT,nbTT))
    resfile.close()
    resToTfile.write("{},{},{},{},{},{}\n".format(test,nbSST,nbSCT,nbCST,nbCCT,nbTT))
resToTfile.close()





