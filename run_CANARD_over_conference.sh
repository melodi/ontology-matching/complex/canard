#!/bin/sh


#ontologies='cmt conference confOf edas ekaw'
#ontologies2='cmt conference confOf edas ekaw'
ontologies='dbpedia'
ontologies2='taxref'

#ontologies='ekaw'
#ontologies2='cmt'
#datasets="100"

#levenshtein="0.0 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1.0"
#levenshtein="0.8 1.0"
#nbInstSupp="1 2 3 4 5 6 7 8 9 20 100"
#nbInstSupp="query"
#nbInstSupp="0.0 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1.0"
nbInstSupp="sim"


log_file=`pwd`/output/logs/
time_file=`pwd`/output/time/


parameters_folder=`pwd`/input/taxon_local/

#for lev in $levenshtein 
#for nsupp in $nbInstSupp
#do
#mkdir `pwd`/output/conference-query/test_$nsupp
#for set in $datasets
#do
  for onto1 in $ontologies
    do
      for onto2 in $ontologies2
       do
         
	 if [ $onto1 != $onto2 ]
	  then
		echo $onto1 - $onto2 _ $lev $nsupp
		((time -p java -Xms2g -jar CANARD.jar $parameters_folder/$onto1-$onto2.json 10 0.4 > $log_file/log_local_$onto1"-"$onto2"_"10.txt)) 2> $time_file/time_local_$onto1"-"$onto2"_10".txt
	fi	
    done
  done
#done
#done
