package fr.irit.sparql.query;

import java.util.AbstractMap;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import fr.irit.resource.IRI;

import java.util.Set;


/**
 * This class allow easy management, preparation and execution of sparql
 * queries.
 */
public abstract class SparqlQuery
{
	private Set<Map.Entry<String, String>> prefix;
	protected String from;
	protected String where;
	protected String mainQuery;
	protected HashMap<String,IRI> iriList;

	public SparqlQuery(Set<Map.Entry<String, String>> prefix, String from, String where)
	{
		this.prefix = new HashSet<>();
		this.prefix.addAll(prefix);
		addDefaultPrefixes();
		this.from = from;
		this.where = where;
		this.iriList =new HashMap<String,IRI>();
	}

	public SparqlQuery(String query){
		this.prefix = new HashSet<>();
		this.iriList =new HashMap<String,IRI>();
		addDefaultPrefixes();
		retrievePrefixes(query);
		this.from="";
		this.where="";
		retrieveIRIs();
	}

	public void addDefaultPrefixes(){
		this.prefix.add(new AbstractMap.SimpleEntry<String, String>("rdf", "http://www.w3.org/1999/02/22-rdf-syntax-ns#"));
		this.prefix.add(new AbstractMap.SimpleEntry<String, String>("rdfs", "http://www.w3.org/2000/01/rdf-schema#"));
		this.prefix.add(new AbstractMap.SimpleEntry<String, String>("owl", "http://www.w3.org/2002/07/owl#"));
		this.prefix.add(new AbstractMap.SimpleEntry<String, String>("xsd", "http://www.w3.org/2001/XMLSchema#"));
		this.prefix.add(new AbstractMap.SimpleEntry<String, String>("skos", "http://www.w3.org/2004/02/skos/core#"));
		this.prefix.add(new AbstractMap.SimpleEntry<String, String>("skos-xl", "http://www.w3.org/2008/05/skos-xl#"));
	}

	public void retrievePrefixes(String aQuery){
		// The first part expands the prefixes of the query
		aQuery = aQuery.trim().replaceAll("PREFIX", "prefix");
		this.mainQuery="";

		// Collect and reduce prefix
		if( aQuery.indexOf("prefix") != -1 )  {
			String[] pref = aQuery.split("prefix");               
			for(int j=0; j < pref.length; j++)  {
				String str = "";
				if(!pref[0].equals(""))   
					str = pref[0];
				else
					str = pref[pref.length-1];
				this.mainQuery = str.substring(str.indexOf('>')+1, str.length());
			}

			for( int i = 0; i < pref.length; i++ )  {   
				String currPrefix = pref[i].trim();       
				if(!currPrefix.equals("") && currPrefix.indexOf('<') != -1 && currPrefix.indexOf('>') != -1)  {
					int begin = currPrefix.indexOf('<');
					int end = currPrefix.indexOf('>');
					String ns = currPrefix.substring(0, currPrefix.indexOf(':')).trim();
					String iri = currPrefix.substring(begin+1, end).trim();
					// Add prefix to list of prefixes
					this.prefix.add(new AbstractMap.SimpleEntry<String, String>(ns, iri));
					this.mainQuery = Pattern.compile(ns+":([A-Za-z0-9_-]+)").matcher(mainQuery).replaceAll("<"+iri+"$1>");
				}
			}
		}
		else {
			this.mainQuery = aQuery;
		}
	}

	public void retrieveIRIs(){
		//Create all the IRIs
		Pattern patternIRI = Pattern.compile("<[^>]+>");
		Matcher matcherIRI = patternIRI.matcher(this.mainQuery);
		while (matcherIRI.find()){
			//System.out.println(matcherIRI.group());
			if(!matcherIRI.group().equals("<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>")) {
				IRI iri = new IRI(matcherIRI.group());
				this.iriList.put(matcherIRI.group(),iri);
			}
		}

		//Add suffix value as label of these IRIs (when the prefix is given)
		for ( Map.Entry<String, String> m : this.prefix ) {
			if ( m.getKey() != null ) {
				Pattern patt = Pattern.compile("<"+m.getValue()+"([^>]+)>");
				Matcher match = patt.matcher(this.mainQuery);
				while (match.find()){
					//System.out.println(match.group(1));
					if(!match.group().equals("<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>")) {
						this.iriList.get(match.group()).addLabel(match.group(1));	
					}		    	
				}

			}
		}

	}

	public String getMainQueryWithPrefixes(){
		String query = this.mainQuery;
		for ( Map.Entry<String, String> m : this.prefix ) {
			Matcher matcher = Pattern.compile("<"+m.getValue()+"([A-Za-z0-9_-]+)>").matcher(query);
			if (matcher.find()){
				query = matcher.replaceAll( m.getKey()+":$1" );
				query = "PREFIX "+m.getKey()+": <"+m.getValue()+"> \n" + query;
			}
			Matcher matcher2 = Pattern.compile(m.getKey()+":([A-Za-z0-9_-]+)").matcher(query);
			if (matcher2.find()){
				query = "PREFIX "+m.getKey()+": <"+m.getValue()+"> \n" + query;
			}
		}
		return query;
	}

	public Set<Map.Entry<String, String>> getPrefix()
	{
		return prefix;
	}

	public void setPrefix(Set<Map.Entry<String, String>> prefix)
	{
		this.prefix = prefix;
	}

	public void addPrefix(String prefix, String fullName)
	{
		this.prefix.add(new AbstractMap.SimpleEntry<String, String>(prefix, fullName));
	}

	public String getFrom()
	{
		return from;
	}

	public void setFrom(String from)
	{
		this.from = from;
	}

	public String getWhere()
	{
		return where;
	}

	public void setWhere(String where)
	{
		this.where = where;
	}

	public String formatPrefixes()
	{
		String prefixes = "";
		for(Entry<String, String> p : this.getPrefix())
		{
			prefixes += "PREFIX "+p.getKey()+": "+p.getValue()+" \n";
		}
		return prefixes;
	}

	public HashMap<String,IRI> getIRIList() {
		return this.iriList;
	}
	
	public String toUnchangedString() {
		return this.mainQuery;
	}
}
