package fr.irit.resource;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.fasterxml.jackson.databind.JsonNode;

import fr.irit.complex.subgraphs.Triple;
import fr.irit.complex.utils.Utils;
import fr.irit.sparql.Proxy.SparqlProxy;
import fr.irit.sparql.query.Exceptions.SparqlEndpointUnreachableException;
import fr.irit.sparql.query.Exceptions.SparqlQueryMalFormedException;
import fr.irit.sparql.query.Select.SparqlSelect;

public class IRI extends Resource{
	private HashSet<String> labels ;
	private HashSet<Triple> triples;
	private HashSet<IRI> types;
	private boolean labelsGot ;
	private boolean triplesRetrieved;

	public IRI (String iri){
		super(iri);
		this.labels = new HashSet<String>();
		this.triples = new HashSet<Triple>();
		this.types = new HashSet<IRI>();
		this.labelsGot = false;
		this.triplesRetrieved = false;
	}

	public void retrieveLabels(String endpointUrl) throws SparqlQueryMalFormedException, SparqlEndpointUnreachableException{
		if(!labelsGot) {
			this.addLabel(this.getSuffix());

			Map<String, String> substitution = new HashMap<>();
			substitution.put("uri", this.value);
			String litteralQuery = Utils.getInstance().getLabelQuery(endpointUrl,substitution);

			SparqlProxy spIn = SparqlProxy.getSparqlProxy(endpointUrl);

			ArrayList<JsonNode> ret = spIn.getResponse(litteralQuery);

			Iterator<JsonNode> retIterator = ret.iterator();
			while (retIterator.hasNext()) {
				String s = retIterator.next().get("x").get("value").toString().replaceAll("\"", "");
				Resource res = new Resource(s);
				//System.out.println(s + " "+ res.isIRI());
				if(!res.isIRI()){
					this.addLabel(s);
				}

			}
			labelsGot=true;
		}

		//System.out.println(this.getNumberOfLabels() + " labels found for " + this.value);
	}

	public void retrieveTypes(String endpointUrl) throws SparqlQueryMalFormedException, SparqlEndpointUnreachableException{
		String query = "SELECT DISTINCT ?type WHERE {" +
				this.value +" a ?type."
				+ "filter(isIRI(?type))}";
		SparqlSelect sq = new SparqlSelect(query);

		SparqlProxy spIn = SparqlProxy.getSparqlProxy(endpointUrl);

		ArrayList<JsonNode> ret = spIn.getResponse(sq.getMainQueryWithPrefixes());

		Iterator<JsonNode> retIterator = ret.iterator();
		while (retIterator.hasNext()) {
			String s = retIterator.next().get("type").get("value").toString().replaceAll("\"", "");
			this.types.add(new IRI ("<"+s+">"));
			//System.out.println(s);
		}
		for (IRI type: types){
			type.retrieveLabels(endpointUrl);
		}
	}

	public IRI findMostSimilarType(String endpointUrl, HashSet<String> targetLabels, double threshold) {
		if (this.getTypes().isEmpty()) {
			try {
				this.retrieveTypes(endpointUrl);
			} catch (SparqlQueryMalFormedException | SparqlEndpointUnreachableException e) {
				e.printStackTrace();
			}
		}
		double scoreTypeMax = -1;
		IRI finalType = null;
		for(IRI type: this.getTypes()){
			double scoreType = 0;
			try {
				type.retrieveLabels(endpointUrl);
			} catch (SparqlQueryMalFormedException | SparqlEndpointUnreachableException e) {
				e.printStackTrace();
			}
			//System.out.println(type.labels);
			scoreType = Utils.similarity(type.getLabels(), targetLabels, threshold);
			if (scoreTypeMax < scoreType){
				scoreTypeMax = scoreType;
				//System.out.println(type+ " "+ scoreTypeMax);
				finalType = type;
			}
		}
		return finalType;
	}

	public void addLabel(String label){
		this.labels.add(label.trim().replaceAll("\\\\", "").toLowerCase());
	}

	public void printLabels(){
		for (String l : this.labels){
			System.out.print(l+ "\t");
		}
		System.out.println();
	}

	public void printSimilar(){
		for (IRI s : this.similarIRIs){
			System.out.print(s+ "\t");
		}
		System.out.println();
	}

	public void findExistingMatches(String sourceEndpoint, String targetEndpoint) throws SparqlQueryMalFormedException, SparqlEndpointUnreachableException{
		ArrayList<IRI> allMatches = new ArrayList<IRI>();
		/**Existing links in source*/
		Map<String, String> substitution = new HashMap<>();
		substitution.put("uri", this.value.replaceAll("\\$", ""));
		String query = Utils.getInstance().getMatchedURIs(sourceEndpoint,substitution);
		SparqlProxy spIn = SparqlProxy.getSparqlProxy(sourceEndpoint);
		ArrayList<JsonNode> ret = spIn.getResponse(query);
		allMatches.add(this);

		Iterator<JsonNode> retIterator = ret.iterator();
		while (retIterator.hasNext()) {
			String s = retIterator.next().get("x").get("value").toString().replaceAll("\"", "");
			Resource res = new Resource (s);
			if(res.isIRI()){
				allMatches.add(new IRI("<"+s+">"));
			}
		}


		/**Existing links in target*/
		String queryTarg = Utils.getInstance().getMatchedURIs(targetEndpoint,substitution);
		SparqlProxy spTarg = SparqlProxy.getSparqlProxy(targetEndpoint);
		ArrayList<JsonNode> retTarg = spTarg.getResponse(queryTarg);

		Iterator<JsonNode> retIteratorTarg = retTarg.iterator();
		while (retIteratorTarg.hasNext()) {
			String s = retIteratorTarg.next().get("x").get("value").toString().replaceAll("\"", "");
			Resource res = new Resource (s);
			if(res.isIRI()){
				allMatches.add(new IRI("<"+s+">"));
			}
		}
		
		/**Check if a match is in the target dataset*/
		for (IRI match: allMatches){
			if (existsInTarget(match,targetEndpoint)){
				this.similarIRIs.add(match);
			}
		}

	}

	/**Check if an IRI is in the target dataset*/
	public boolean existsInTarget(IRI match, String targetEndpoint) throws SparqlQueryMalFormedException, SparqlEndpointUnreachableException{
		String queryMatch = "ASK {" +
				"{"+match.toString()+" ?z ?x. "
				+ "MINUS {"+match.toString()+" <http://www.w3.org/2002/07/owl#sameAs> ?x.}"
				+ "MINUS{ "+match.toString()+" <http://www.w3.org/2004/02/skos/core#closeMatch> ?x.}"
				+ "MINUS{ "+match.toString()+"  <http://www.w3.org/2004/02/skos/core#exactMatch> ?x.}"
				+ "}" +
				"UNION{?a ?b " +match.toString()+". "
				+ "MINUS { ?a <http://www.w3.org/2002/07/owl#sameAs> "+match.toString()+"}"
				+ "MINUS { ?a <http://www.w3.org/2004/02/skos/core#exactMatch> "+match.toString()+"}"
				+ "MINUS { ?a <http://www.w3.org/2004/02/skos/core#closeMatch> "+match.toString()+"}"
				+"}" +
				"}";
		SparqlProxy spTarg = SparqlProxy.getSparqlProxy(targetEndpoint);
		return spTarg.sendAskQuery(queryMatch);
		//System.out.println(match + " " + matchInTarget);

	}


	public void findSimilarResource(String targetEndpoint) {
		//	System.out.println("similar to "+ this.toString());
		if (this.labels.isEmpty()) {
			try {
				this.retrieveLabels(targetEndpoint);
			} catch (SparqlQueryMalFormedException | SparqlEndpointUnreachableException e) {
				e.printStackTrace();
			}
		}
		//Label search (heavy on large KBs)
		for (String rawLab: this.labels){
			
			String label = rawLab.replaceAll("[\\^\\+\\{\\}\\.\\?]", "");
			//System.out.println(label);
			Map<String, String> substitution = new HashMap<>();
			substitution.put("labelValue", label.toLowerCase());
			if (label.length()>1){
				substitution.put("LabelValue", label.substring(0, 1).toUpperCase() + label.substring(1));
			}
			else{
				substitution.put("LabelValue", label.toUpperCase());
			}
			String litteralQuery = Utils.getInstance().getSimilarQuery(targetEndpoint,substitution);

			if (litteralQuery.length()<2000){
				SparqlProxy spIn = SparqlProxy.getSparqlProxy(targetEndpoint);
				
				ArrayList<JsonNode> ret;
				try {
					ret = spIn.getResponse(litteralQuery);
					Iterator<JsonNode> retIterator = ret.iterator();
					while (retIterator.hasNext()) {
						String s = retIterator.next().get("x").get("value").toString().replaceAll("\"", "");
						similarIRIs.add(new IRI("<"+s+">"));
					}
				} catch (SparqlQueryMalFormedException | SparqlEndpointUnreachableException e) {
					e.printStackTrace();
				}

				

				/*substitution.put("labelValue", "\""+this.value.substring(0, 1).toUpperCase() + this.value.substring(1)+"\"@en");
				litteralQuery = Utils.getInstance().getSimilarQuery(targetEndpoint,substitution);
				ret = spIn.getResponse(litteralQuery);

				retIterator = ret.iterator();
				while (retIterator.hasNext()) {
					String s = retIterator.next().get("x").get("value").toString().replaceAll("\"", "");
					//System.out.println(s);
					similarIRIs.add(new IRI("<"+s+">"));
				}

				substitution.put("labelValue", "\""+this.value.substring(0, 1).toUpperCase() + this.value.substring(1)+"\"");
				litteralQuery = Utils.getInstance().getSimilarQuery(targetEndpoint,substitution);
				ret = spIn.getResponse(litteralQuery);

				retIterator = ret.iterator();
				while (retIterator.hasNext()) {
					String s = retIterator.next().get("x").get("value").toString().replaceAll("\"", "");
					//System.out.println(s);
					similarIRIs.add(new IRI("<"+s+">"));
				}*/
			}
		}
	}

	public void retrieveAllTriples(String targetEndpoint) throws SparqlQueryMalFormedException, SparqlEndpointUnreachableException{
		if(!this.triplesRetrieved) {
			getSubjectTriples(targetEndpoint);
			getObjectTriples(targetEndpoint);
			getPredicateTriples(targetEndpoint);
			this.triplesRetrieved = true;
		}		
	}

	private void getPredicateTriples(String targetEndpoint) throws SparqlQueryMalFormedException, SparqlEndpointUnreachableException {
		String query = "SELECT ?subject ?object WHERE {" +
				"?subject "+this.value+" ?object."
				+ "} LIMIT 500";

		SparqlProxy spIn = SparqlProxy.getSparqlProxy(targetEndpoint);

		ArrayList<JsonNode> ret = spIn.getResponse(query);

		Iterator<JsonNode> retIterator = ret.iterator();
		while (retIterator.hasNext()) {
			JsonNode response = retIterator.next();
			String sub = response.get("subject").get("value").toString().replaceAll("\"", "");
			String obj = response.get("object").get("value").toString().replaceAll("\"", "");
			this.triples.add(new Triple("<"+sub+">",this.value,obj,2));
			//   System.out.println(sub + " "+ obj);
		}

	}

	private void getObjectTriples(String targetEndpoint) throws SparqlQueryMalFormedException, SparqlEndpointUnreachableException {
		String query = "SELECT ?subject ?predicate WHERE {" +
				"?subject ?predicate "+this.value+"."
				+ "MINUS{ ?subject <http://www.w3.org/2002/07/owl#sameAs> "+this.value+".}"
				+ "MINUS{ ?subject <http://www.w3.org/2004/02/skos/core#closeMatch> "+this.value+".}"
				+ "MINUS{ ?subject <http://www.w3.org/2004/02/skos/core#exactMatch> "+this.value+".}"
				+ "}LIMIT 500";

		SparqlProxy spIn = SparqlProxy.getSparqlProxy(targetEndpoint);

		ArrayList<JsonNode> ret = spIn.getResponse(query);

		Iterator<JsonNode> retIterator = ret.iterator();
		while (retIterator.hasNext()) {
			JsonNode response = retIterator.next();
			String sub = response.get("subject").get("value").toString().replaceAll("\"", "");
			String pred = response.get("predicate").get("value").toString().replaceAll("\"", "");
			// System.out.println(sub + " "+ pred);
			this.triples.add(new Triple("<"+sub+">","<"+pred+">",this.value,3));
		}

	}

	private void getSubjectTriples(String targetEndpoint) throws SparqlQueryMalFormedException, SparqlEndpointUnreachableException {
		String query = "SELECT ?predicate ?object WHERE {" +
				this.value + " ?predicate ?object."
				+ "MINUS{ "+this.value+" <http://www.w3.org/2002/07/owl#sameAs> ?object.}"

				+ "}LIMIT 500";

		SparqlProxy spIn = SparqlProxy.getSparqlProxy(targetEndpoint);

		ArrayList<JsonNode> ret = spIn.getResponse(query);

		Iterator<JsonNode> retIterator = ret.iterator();
		while (retIterator.hasNext()) {
			JsonNode response = retIterator.next();
			if(!response.get("object").get("value").toString().matches("\"b[0-9]+\"")) {
				String pred = response.get("predicate").get("value").toString().replaceAll("\"", "");
				String obj = response.get("object").get("value").toString().replaceAll("\"", "");
				//  System.out.println(pred + " "+ obj);
				this.triples.add(new Triple(this.value,"<"+pred+">",obj,1));
			}
			
		}

	}

	public HashSet<String> getLabels(){
		return this.labels;
	}

	public HashSet<Triple> getTriples(){
		return this.triples;
	}

	public int getNumberOfLabels(){
		return this.labels.size();
	}

	public HashSet<IRI> getTypes(){
		return this.types;
	}

	public String getSuffix(){
		Pattern pattern = Pattern.compile("<([^>]+)[#/]([A-Za-z0-9_-]+)>");
		Matcher matcher = pattern.matcher(value);
		if (matcher.find()){
			//System.out.println(value +" "+matcher.group(2));
			return matcher.group(2);
		}
		else{
			return value;
		}
	}

	public String toStrippedString() {
		return this.value.replaceAll("<", "").replaceAll(">", "");
	}

	public Triple findTripleWithMostSimilarResource(String targetEndpoint, HashSet<String> resourceLabels,double threshold, double thresholdRes) {
		double maxSim = -1;
		Triple tMax= null;
		for(Triple t: this.triples) {
			try {
				double similarity=-1;
				t.retrieveIRILabels(targetEndpoint);
				if (t.isSubjectTriple()) {
					if(t.getObject() instanceof IRI) {
						similarity = Utils.similarity(((IRI)t.getObject()).getLabels(), resourceLabels, threshold);
					}
					else {
						HashSet<String> rLabels = new HashSet<String>();
						rLabels.add(t.getObject().value);
						similarity = Utils.similarity(rLabels, resourceLabels, threshold);
					}
				}
				if (t.isObjectTriple()) {
					similarity = Utils.similarity(((IRI)t.getSubject()).getLabels(), resourceLabels, threshold);
				}
				
				if(similarity > maxSim) {
					maxSim=similarity;
					tMax = t;
				}
			} catch (SparqlQueryMalFormedException | SparqlEndpointUnreachableException e) {
				e.printStackTrace();
			}

		}
		if (maxSim < thresholdRes) {
			tMax = null;
		}

		return tMax;

	}

}
