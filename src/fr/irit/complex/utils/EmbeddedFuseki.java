package fr.irit.complex.utils;

import java.util.HashMap;
import java.util.Map;

import org.apache.jena.fuseki.embedded.FusekiEmbeddedServer;
import org.apache.jena.query.Dataset;
import org.apache.jena.query.DatasetFactory;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.util.FileManager;

public class EmbeddedFuseki {
    
   static private EmbeddedFuseki fusekiServer;
    static public EmbeddedFuseki getFusekiServer(){
    	//System.out.println("fuseki server there ?");
        if(EmbeddedFuseki.fusekiServer == null){
            EmbeddedFuseki.fusekiServer = new EmbeddedFuseki();
        }
        return EmbeddedFuseki.fusekiServer;
    }
    
    
    private FusekiEmbeddedServer server;
    private FusekiEmbeddedServer.Builder serverBuilder;
    
    private HashMap<String, Dataset> datasets;
    
    public EmbeddedFuseki(){
        this.datasets = new HashMap<>();
    }
    

    
    public void addDataset(String name, String filePath){
    	System.out.println("Loading "+filePath+" into ("+name+") KB...");
    	Model m= ModelFactory.createDefaultModel();
    	//System.out.println("1");
    	Model localKB = FileManager.get().loadModel(filePath);
    	//System.out.println("2");
		m.add(localKB);
		//System.out.println("3");
		Dataset ds = DatasetFactory.create(m);
		//System.out.println("4");
        this.datasets.put(name, ds);
        //System.out.println("5");
    }
    
    public void startServer(){
    	System.out.println("Trying to create the server");
        this.serverBuilder = FusekiEmbeddedServer.create().setPort(3031);
        System.out.println("addition of datasets");
         for(Map.Entry<String, Dataset> e : this.datasets.entrySet()){
            this.serverBuilder.add("/"+e.getKey(), e.getValue());
        }
        this.server = this.serverBuilder.build();
        this.server.start(); 
        System.out.println("Fuseki server started as localhost:"+this.server.getPort());
    }
    
    public void closeConnection(){
        this.server.stop();
    }
}
