package fr.irit.complex.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Iterator;

import com.fasterxml.jackson.databind.JsonNode;

import fr.irit.sparql.Proxy.SparqlProxy;
import fr.irit.sparql.query.Exceptions.SparqlEndpointUnreachableException;
import fr.irit.sparql.query.Exceptions.SparqlQueryMalFormedException;

public class CQAGenerator {

	private String endpoint;
	private String CQAFolder;
	private int count;
	private double ratio;
	private int maxCAV;

	public CQAGenerator(String endpoint, String CQAFolder){
		this.endpoint = endpoint;	
		this.CQAFolder = CQAFolder;
		count=0;
		this.ratio = 30;
		this.maxCAV = 20;
	}

	public void createCQAs(){
		createClasses();
		createCAV();
		createProperties();
	}
	
	public void cleanCQARepository() {
		Path cqaPath = Paths.get(CQAFolder);
		try {
			//If the folder does not exist, create it
			if (Files.notExists(cqaPath)){
				Files.createDirectory(cqaPath);
			}
			//Else empty the folder
			else{
				File dir = new File(CQAFolder);
				for(File file: dir.listFiles()){
					file.delete(); 
				}
			}			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void createClasses(){
		// Get all the classes 
		try {
			String query = "PREFIX owl: <http://www.w3.org/2002/07/owl#>  \n" +
					"SELECT distinct ?x WHERE{  \n" +
					"?x a owl:Class. \n" +
					"?y a ?x."
					+ " filter(isIRI(?x))}";
			SparqlProxy spIn = SparqlProxy.getSparqlProxy(endpoint); 
			ArrayList<JsonNode> ret;
			ret = spIn.getResponse(query);
			Iterator<JsonNode> retIterator = ret.iterator();
			while (retIterator.hasNext()) {
				String owlClass = retIterator.next().get("x").get("value").toString().replaceAll("\"", "");
				if(interestingIRI(owlClass)){
					//System.out.println(owlClass);
					//Create new file in designated folder with the new CQA
					PrintWriter writer = new PrintWriter(new File(this.CQAFolder+"/CQA"+count+".sparql"), "UTF-8");
					String CQA = "SELECT DISTINCT ?x WHERE {  \n" +
							"?x a <"+owlClass+">.} " ;
					writer.append(CQA);
					writer.flush();
					writer.close();
					count ++;
				}
			} 
		} catch (SparqlQueryMalFormedException
				| SparqlEndpointUnreachableException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
	}
	
	public void createProperties(){
		// Get all the classes 
		try {
			String query = "PREFIX owl: <http://www.w3.org/2002/07/owl#>  \n" +
					"SELECT distinct ?x WHERE{  \n" +
					"?y ?x ?z. "
					+ "{?x a owl:ObjectProperty.}\n" + 
					"  union{\n" + 
					"    ?x a owl:DatatypeProperty.}\n" + 
					"  }";
			SparqlProxy spIn = SparqlProxy.getSparqlProxy(endpoint); 
			ArrayList<JsonNode> ret;
			ret = spIn.getResponse(query);
			Iterator<JsonNode> retIterator = ret.iterator();
			while (retIterator.hasNext()) {
				String owlProp = retIterator.next().get("x").get("value").toString().replaceAll("\"", "");
				if(interestingIRI(owlProp)){
					//System.out.println(owlClass);
					//Create new file in designated folder with the new CQA
					PrintWriter writer = new PrintWriter(new File(this.CQAFolder+"/CQA"+count+".sparql"), "UTF-8");
					String CQA = "SELECT DISTINCT ?x ?y WHERE {  \n" +
							"?x <"+owlProp+"> ?y.} " ;
					writer.append(CQA);
					writer.flush();
					writer.close();
					count ++;
				}
			} 
		} catch (SparqlQueryMalFormedException
				| SparqlEndpointUnreachableException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
	}

	public void createCAV(){
		try {
			//Get all "interesting" properties
			String query = "PREFIX owl: <http://www.w3.org/2002/07/owl#>   \n" +
					"SELECT distinct ?x WHERE {   \n" +
					"?x a owl:ObjectProperty.}" ;
			SparqlProxy spIn = SparqlProxy.getSparqlProxy(endpoint); 
			ArrayList<JsonNode> ret = spIn.getResponse(query);
			Iterator<JsonNode> retIterator = ret.iterator();
			while (retIterator.hasNext()) {
				String property = retIterator.next().get("x").get("value").toString().replaceAll("\"", "");
				//For each interesting object property P, 
				if(interestingIRI(property)){
					//System.out.println(property);
					//count the number of subjects and objects
					String queryNb = "SELECT (count(distinct ?x) as ?sub) (count(distinct ?y) as ?ob) where {\n" +
							"?x <"+property+"> ?y.}";
					ArrayList<JsonNode> retNb = spIn.getResponse(queryNb);
					Iterator<JsonNode> retNbIterator = retNb.iterator();
					while (retNbIterator.hasNext()) {
						JsonNode nodeNb = retNbIterator.next();

						int nbSub = Integer.parseInt(nodeNb.get("sub").get("value").toString().replaceAll("\"", ""));
						int nbOb = Integer.parseInt(nodeNb.get("ob").get("value").toString().replaceAll("\"", ""));

						if (nbSub != 0 && nbOb !=0){
							// If n_subj >> n_obj and n_obj < maxThreshold
							if ((double)nbSub/(double)nbOb > this.ratio && nbOb < this.maxCAV){
								// get all the objects
								String queryOb = "SELECT distinct ?y where {\n" +
										"?x <"+property+"> ?y.}";
								ArrayList<JsonNode> retOb = spIn.getResponse(queryOb);
								Iterator<JsonNode> retObIterator = retOb.iterator();
								// create n_obj CAV: ?x P oi
								while (retObIterator.hasNext()) {								
									String object =retObIterator.next().get("y").get("value").toString().replaceAll("\"", "");
									PrintWriter writer = new PrintWriter(new File(this.CQAFolder+"/CQA"+count+".sparql"), "UTF-8");
									String CQA = "SELECT DISTINCT ?x WHERE {\n" +
											"?x <"+property+"> <"+object+">.} " ;
									writer.append(CQA);
									writer.flush();
									writer.close();
									count ++;
								}

							}

							// ELIF n_obj >> n_subj and n_subj < threshold
							else if ((double)nbSub/(double)nbOb > this.ratio && nbOb < this.maxCAV){
								//get all the subjects
								String querySub = "SELECT distinct ?x where {\n" +
										"?x <"+property+"> ?y.}";
								ArrayList<JsonNode> retSub = spIn.getResponse(querySub);
								Iterator<JsonNode> retSubIterator = retSub.iterator();
								while (retSubIterator.hasNext()) {
									// create n_subj CIAV: si P ?x
									String subject =retSubIterator.next().get("x").get("value").toString().replaceAll("\"", "");
									PrintWriter writer = new PrintWriter(new File(this.CQAFolder+"/CQA"+count+".sparql"), "UTF-8");
									String CQA = "SELECT DISTINCT ?x WHERE {\n" +
											"<"+subject+"> <"+property+"> ?x.} ";
									writer.append(CQA);
									writer.flush();
									writer.close();
									count ++;
								}
							}
						}
					}
				}
			} 
		} catch (SparqlQueryMalFormedException
				| SparqlEndpointUnreachableException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}

	}

	public boolean interestingIRI(String iri){
		return !(iri.contains("http://www.w3.org/2000/01/rdf-schema#") ||
				iri.contains("http://www.w3.org/1999/02/22-rdf-syntax-ns#") ||
				iri.contains("http://www.w3.org/2001/XMLSchema#") ||
				iri.contains("http://www.w3.org/2004/02/skos/core#") ||
				iri.contains("http://www.w3.org/2008/05/skos-xl#") ||
				iri.contains("http://www.w3.org/2002/07/owl#") ||
				iri.contains("http://xmlns.com/foaf/") ||
				iri.contains("http://purl.org/dc/terms/") ||
				iri.contains("http://purl.org/dc/elements/1.1/"));
	}
}
