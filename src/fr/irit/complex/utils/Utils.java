package fr.irit.complex.utils;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.semanticweb.owl.align.AlignmentException;

import com.fasterxml.jackson.core.JsonProcessingException;

import fr.irit.complex.subgraphs.SubgraphForOutput;
import fr.irit.resource.IRI;
import fr.irit.sparql.exceptions.IncompleteSubstitutionException;
import fr.irit.sparql.exceptions.NotAFolderException;
import fr.irit.sparql.query.Exceptions.SparqlEndpointUnreachableException;
import fr.irit.sparql.query.Exceptions.SparqlQueryMalFormedException;
import fr.irit.sparql.query.Select.SparqlSelect;

public final class Utils {
	public static final Utils instance = new Utils();

	private Parameters params;
	private ArrayList<Output> outputs;


	private Utils(){}

	public static Utils getInstance() {
		return instance;
	}


	/**INIT WITH PARAMETERS
	 * @throws SparqlEndpointUnreachableException 
	 * @throws SparqlQueryMalFormedException 
	 * @throws AlignmentException 
	 * @throws URISyntaxException */
	public boolean init(String filePath) throws JsonProcessingException, IOException, NotAFolderException, SparqlQueryMalFormedException, SparqlEndpointUnreachableException, URISyntaxException, AlignmentException{
		params = new Parameters();
		boolean ret = this.params.init(filePath);
		outputs = new ArrayList<Output>();
		if (this.params.outputEDOAL){
			outputs.add(new EDOALOutput(params));
		}
		if(this.params.outputQUERY){
			outputs.add(new QueryOutput(params));

		}
		if (this.params.outputSPARQL){
			outputs.add(new SPARQLOutput(params));
		}
		
		for (Output o: outputs){
			o.init();
		}

		return ret;

	}
	
	public static HashSet<String> getQueryLabels(SparqlSelect query){
		HashSet<String> queryLabels = new HashSet<String>();
		

		/**Get Query labels*/
		//System.out.println("Labels : ");
		for (Map.Entry<String,IRI> iri : query.getIRIList().entrySet()){
			for (String label : iri.getValue().getLabels()){
				//System.out.println(label);
				queryLabels.add(label);
			}
		}
		return queryLabels;
	}


	/**SIMILARITY functions*/
	public static double similarity(HashSet<String> labels1, HashSet<String> labels2, double threshold){
		double score = 0;
		for(String l1 : labels1){
			for(String l2: labels2){
				score+=(stringSimilarity(l1, l2,threshold));
			}
		}
		return score;
	}


	public static double stringSimilarity(String s1, String s2,double threshold){
		double dist = StringUtils.getLevenshteinDistance(s1.toLowerCase(), s2.toLowerCase())/((double)Math.max(s1.length(), s2.length()));
		double sim = 1- dist;

		/*if ((s1.contains(s2) || s2.contains(s1) )&& sim <1){
			sim += 0.1;
		}*/

		if (sim < threshold){
			return 0;
		}
		else{
			return sim;
		}
	}


	/**QUERIES*/

	public String getLabelQuery(String endpoint,Map<String, String> substitution){
		String query = "";
		try {
			query = this.params.queryTemplates.get(endpoint).getTemplateQueries().get("labels").substitute(substitution);
		} catch (IncompleteSubstitutionException e) {
			e.printStackTrace();
		}
		return query;
	}

	public String getSimilarQuery(String endpoint,Map<String, String> substitution){
		String query = "";
		try {
			query = this.params.queryTemplates.get(endpoint).getTemplateQueries().get("similar").substitute(substitution);
		} catch (IncompleteSubstitutionException e) {
			e.printStackTrace();
		}
		return query;
	}

	public String getMatchedURIs(String endpoint,Map<String, String> substitution){
		String query = "";
		try {			
			query = this.params.queryTemplates.get(endpoint).getTemplateQueries().get("matched").substitute(substitution);
		} catch (IncompleteSubstitutionException e) {
			e.printStackTrace();
		}
		return query;
	}





	/**OUTPUT*/	
	public void addToOutput(SparqlSelect sq, ArrayList<SubgraphForOutput> subGraph){
		for(Output o: outputs){
			o.addToOutput(subGraph, sq);
		}

	}


	/**END*/
	public void end(){
		//close embedded fuseki
		if(this.params.startEmbeddedFuseki){
			EmbeddedFuseki fusekiServer = EmbeddedFuseki.getFusekiServer();
			fusekiServer.closeConnection();
			
		}
		/*if(this.params.cqaToBeGenerated) {
			CQAGenerator generator = new CQAGenerator (this.params.sourceEndpoint, this.params.CQAFolder);
			generator.cleanCQARepository();
		}*/


		for(Output o: outputs){
			o.end();
		}
	}

	/**Getters*/
	public String getSourceEndpoint(){
		return this.params.sourceEndpoint;
	}

	public String getTargetEndpoint(){
		return this.params.targetEndpoint;
	}

	public ArrayList<SparqlSelect> getQueries(){
		return this.params.queries;
	}


}
