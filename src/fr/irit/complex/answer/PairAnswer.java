package fr.irit.complex.answer;

import java.util.ArrayList;
import java.util.HashSet;

import fr.irit.complex.subgraphs.InstantiatedSubgraph;
import fr.irit.complex.subgraphs.Path;
import fr.irit.complex.utils.Utils;
import fr.irit.resource.IRI;
import fr.irit.resource.Resource;
import fr.irit.sparql.query.Exceptions.SparqlEndpointUnreachableException;
import fr.irit.sparql.query.Exceptions.SparqlQueryMalFormedException;
import fr.irit.sparql.query.Select.SparqlSelect;

public class PairAnswer extends Answer{
	Resource r1;
	Resource r2;
	boolean similarlooked;

	public PairAnswer(Resource r1, Resource r2) {
		if(r1.isIRI()){
			this.r1 = new IRI("<"+r1.toString()+">");
		}
		else{
			this.r1= r1;
		}
		if(r2.isIRI()){
			this.r2 = (IRI) new IRI("<"+r2.toString()+">");
		}
		else{
			this.r2= r2;
		}
		similarlooked=false;
	}

	public void retrieveIRILabels(String endpointURL) throws SparqlQueryMalFormedException, SparqlEndpointUnreachableException{
		if (this.r1 instanceof IRI){
			((IRI)this.r1).retrieveLabels(endpointURL);
			//((IRI)this.r1).printLabels();			
		}
		if (this.r2 instanceof IRI){
			((IRI)this.r2).retrieveLabels(endpointURL);
			//((IRI)this.r2).printLabels();			
		}

	}

	public void getSimilarIRIs(String targetEndpoint, String sourceEndpoint) throws SparqlQueryMalFormedException, SparqlEndpointUnreachableException{
		//this.getExistingMatches(sourceEndpoint, targetEndpoint);
		if(!similarlooked) {
			if (this.r1 instanceof IRI){
				((IRI)this.r1).findSimilarResource(targetEndpoint);
			}
			if (this.r2 instanceof IRI){
				((IRI)this.r2).findSimilarResource(targetEndpoint);
			}
			similarlooked=true;
		}
	}

	public void getExistingMatches(String sourceEndpoint, String targetEndpoint) throws SparqlQueryMalFormedException, SparqlEndpointUnreachableException{

		if (this.r1 instanceof IRI){
			((IRI)this.r1).findExistingMatches(sourceEndpoint, targetEndpoint);
		}
		if (this.r2 instanceof IRI){
			((IRI)this.r2).findExistingMatches(sourceEndpoint, targetEndpoint);
		}

	}

	public HashSet<InstantiatedSubgraph> findCorrespondingSubGraph(SparqlSelect query, String targetEndpoint, double similarityThreshold, String sourceEndpoint ) throws SparqlQueryMalFormedException, SparqlEndpointUnreachableException {		
		//				System.out.println("Les synonymes de "+this.r1.toValueString() + " "+this.r2.toValueString());
		//				for(Resource sim: this.r1.getSimilarIRIs()){
		//					System.out.println(sim.toString());
		//				}
		//				for(Resource sim: this.r2.getSimilarIRIs()){
		//					System.out.println(sim.toString());
		//				}
		HashSet<String> queryLabels = Utils.getQueryLabels(query);
		HashSet<InstantiatedSubgraph> paths = new HashSet<InstantiatedSubgraph>();

		//If R1 and R2 have a match
		if (this.hasTotalMatch()) {
			for (Resource x: r1.getSimilarIRIs()) {
				for (Resource y: r2.getSimilarIRIs()) {
					int length=1;
					boolean found = false;
					// Find a path between x and y of length between 1 and 3
					while (length< 4 && !found) {
						ArrayList<ArrayList<Boolean>> allInv= allInversePossibilities(length);
						for(ArrayList<Boolean> invArray : allInv) {
							Path p = new Path(x,y,targetEndpoint,length,invArray);
							if (p.pathFound()) {
								p.getMostSimilarTypes(targetEndpoint, queryLabels, 0.0);
								paths.add(p);
								found = true;
							}
						}						
						length ++;
					}
				}
			}
		}

		// if no path was found and r1 has a match
		if (paths.isEmpty() && this.hasR1Match()) {
			if(!r2.isIRI()) {
				for (IRI x: r1.getSimilarIRIs()) {
					int length=1;
					boolean found = false;

					while (length< 4 && !found) {
						ArrayList<ArrayList<Boolean>> allInv= allInversePossibilities(length);
						for(ArrayList<Boolean> invArray : allInv) {
							Path p = new Path(x,r2,targetEndpoint,length,invArray);
							if (p.pathFound()) {
								p.getMostSimilarTypes(targetEndpoint, queryLabels, 0.0);
								paths.add(p);
								found = true;
							}
						}						
						length ++;
					}
				}

			}
			/*	else {
				for (IRI x: r1.getSimilarIRIs()) {

					x.retrieveAllTriples(targetEndpoint);
					// compare the triples with r2
					HashSet<String> r2Labels = new HashSet<String>();
					if(r2 instanceof IRI) {
						r2Labels.addAll(((IRI)r2).getLabels());
					}
					else {
						r2Labels.add(r2.toString());
					}

					Triple t = x.findTripleWithMostSimilarResource(targetEndpoint, r2Labels, similarityThreshold,0.9);
					if(t!=null) {
						Resource y = null;
						if(t.isSubjectTriple()) {
							y = t.getObject();
						}
						else if(t.isObjectTriple()) {
							y = t.getSubject();
						}
						Path p = new Path(t,x,y);
						p.getMostSimilarTypes(targetEndpoint, queryLabels, 0.0);
						paths.add(p);
					}				
				}
			}*/
		}

		// if no path was found and r2 has a match
		if (paths.isEmpty() && this.hasR2Match()) {
			if(!r1.isIRI()) {
				for (IRI y: r2.getSimilarIRIs()) {	
					int length=1;
					boolean found = false;

					while (length< 4 && !found) {
						ArrayList<ArrayList<Boolean>> allInv= allInversePossibilities(length);
						for(ArrayList<Boolean> invArray : allInv) {
							Path p = new Path(r1,y,targetEndpoint,length,invArray);
							if (p.pathFound()) {
								p.getMostSimilarTypes(targetEndpoint, queryLabels, 0.0);
								paths.add(p);
								found = true;
							}
						}						
						length ++;
					}
				}

			}
			/*else {
				for (IRI y: r2.getSimilarIRIs()) {
					y.retrieveAllTriples(targetEndpoint);
					// compare the triples with r2
					HashSet<String> r1Labels = new HashSet<String>();
					if(r1 instanceof IRI) {
						r1Labels.addAll(((IRI)r1).getLabels());
					}
					else {
						r1Labels.add(r1.toString());
					}

					Triple t = y.findTripleWithMostSimilarResource(targetEndpoint, r1Labels, similarityThreshold,0.9);
					if(t!=null) {
						Resource x = null;
						if(t.isSubjectTriple()) {
							x = t.getObject();
						}
						else if(t.isObjectTriple()) {
							x = t.getSubject();
						}
						Path p = new Path(t,x,y);
						p.getMostSimilarTypes(targetEndpoint, queryLabels, 0.0);
						paths.add(p);
					}				
				}
			}*/

		}


		// For each path, add a similarity
		for (InstantiatedSubgraph p: paths) {
			if (p instanceof Path) {
				((Path) p).compareLabel(queryLabels, similarityThreshold, targetEndpoint,0.5);
				//System.out.println(((Path) p).toSubGraphString());
			}
			else {
				System.err.println("problem in Pair answer: instantiated subgraph is not a path...");
			}
		}
		if (paths.isEmpty() && !similarlooked) {
			this.getSimilarIRIs(targetEndpoint, sourceEndpoint);
			System.out.println("No path found, similar answers : "+ this.printMatchedEquivalents());
			paths = this.findCorrespondingSubGraph(query,targetEndpoint,similarityThreshold,sourceEndpoint);
		}

		return paths;
	}

	// has at least one match (r1 or r2)
	public boolean hasMatch(){ 
		boolean match = true;
		if(r1.isIRI() && !this.hasR1Match()) {
			match= false;
		}
		if(r2.isIRI() && !this.hasR2Match()) {
			match= false;
		}
		return match;
		//return this.hasR1Match() || hasR2Match();
		//return hasTotalMatch();
	}

	private boolean hasR1Match() {
		return !this.r1.getSimilarIRIs().isEmpty();
	}

	private boolean hasR2Match() {
		return !this.r2.getSimilarIRIs().isEmpty();
	}

	private boolean hasTotalMatch() {
		return hasR1Match() && hasR2Match();
	}

	public String toString() {
		return this.r1.toString() + " " + this.r2.toString();
	}

	private ArrayList<ArrayList<Boolean>> allInversePossibilities(int length){
		ArrayList<ArrayList<Boolean>> result = new ArrayList<ArrayList<Boolean>>();
		for(int i =0; i< Math.pow(2, length); i++) {
			ArrayList<Boolean>invArray = new ArrayList<Boolean>();
			String invStr = Integer.toBinaryString(i);
			while(invStr.length()<length) {
				invStr = "0"+invStr;
			}
			for(char invCh: invStr.toCharArray()) {
				if(invCh == '0') {
					invArray.add(false);
				}
				else if (invCh == '1') {
					invArray.add(true);
				}
			}
			//.println(invArray);
			result.add(invArray);
		}


		return result;

	}

	public String printMatchedEquivalents() {
		return this.r1.getSimilarIRIs().toString() +" <--> "+ this.r2.getSimilarIRIs().toString();
	}

}
