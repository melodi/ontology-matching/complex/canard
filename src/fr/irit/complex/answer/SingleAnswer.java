package fr.irit.complex.answer;

import java.util.ArrayList;
import java.util.HashSet;

import fr.irit.complex.subgraphs.InstantiatedSubgraph;
import fr.irit.complex.subgraphs.Triple;
import fr.irit.complex.utils.Utils;
import fr.irit.resource.IRI;
import fr.irit.resource.Resource;
import fr.irit.sparql.query.Exceptions.SparqlEndpointUnreachableException;
import fr.irit.sparql.query.Exceptions.SparqlQueryMalFormedException;
import fr.irit.sparql.query.Select.SparqlSelect;

public class SingleAnswer extends Answer{
	Resource res;
	int numberMaxOfExploredAnswers;


	public SingleAnswer(Resource r){
		super();
		if(r.isIRI()){
			this.res = new IRI("<"+r.toString()+">");
		}
		else{
			this.res= r;
		}
		this.numberMaxOfExploredAnswers = 20;
	}

	public void retrieveIRILabels(String endpointURL) throws SparqlQueryMalFormedException, SparqlEndpointUnreachableException{
		if (this.res instanceof IRI){
			((IRI)this.res).retrieveLabels(endpointURL);
			//((IRI)this.r1).printLabels();	
		}
	}

	//find similarIRIs only if no matches were found
	public void getSimilarIRIs(String targetEndpoint, String sourceEndpoint) throws SparqlQueryMalFormedException, SparqlEndpointUnreachableException{
		if (this.res.getSimilarIRIs().isEmpty()){
			this.res.findSimilarResource(targetEndpoint);
		}
	}

	public void getExistingMatches(String sourceEndpoint, String targetEndpoint) throws SparqlQueryMalFormedException, SparqlEndpointUnreachableException{
		if (this.res instanceof IRI){
			((IRI)this.res).findExistingMatches(sourceEndpoint, targetEndpoint);
			//((IRI)this.res).printSimilar();
		}
	}

	public String answerType(){
		if(this.res instanceof IRI){
			return "IRI";
		}
		else{
			return "value";
		}
	}

	public HashSet<InstantiatedSubgraph> findCorrespondingSubGraph(SparqlSelect query, String targetEndpoint, double similarityThreshold, String sourceEndpoint) throws SparqlQueryMalFormedException, SparqlEndpointUnreachableException{
		//		System.out.println("Labels of " + this.res.value);
		//		((IRI)this.res).printLabels();

		//		System.out.println("Les synonymes de "+this.res.value);
		//		((IRI)this.res).printSimilar();

		HashSet<String> queryLabels = Utils.getQueryLabels(query);
		double maxSim = -1;
		Triple bestTriple = new Triple();
		/**For each similar IRI
		 * Get DESCRIBE of IRI
		 * Find in DESCRIBE triples similar string with query's IRIs labels
		 * If found, keep triple as subGraph*/
		HashSet<InstantiatedSubgraph> goodTriples = new HashSet<InstantiatedSubgraph>();


		//limit the number of similarIRIs parsed
		int count = 0;
		for(IRI iri : this.res.getSimilarIRIs()){
			if (count < this.numberMaxOfExploredAnswers){
				count ++;
				//System.out.println("Getting triples ...");
				ArrayList<Triple> localGoodTriples = new ArrayList<Triple>();
				double localMaxSim = -1;
				//System.out.println(iri.value);
				iri.retrieveAllTriples(targetEndpoint);
				for (Triple t: iri.getTriples()){
					double similarity = 0;
					t.retrieveIRILabels(targetEndpoint);
					//System.out.println("in the loop");
					t.retrieveTypes(targetEndpoint);
					//t.printLabels();
					// threshold = percentage of similarity for acceptance
					similarity += t.compareLabel(queryLabels,similarityThreshold,targetEndpoint);

					/**Global max Sim*/
					if (similarity > maxSim){
						maxSim = similarity;
						bestTriple = t;
					}

					/**Local Max Sim*/
					if (similarity > localMaxSim){
						localMaxSim = similarity;
						//System.out.println(similarity + " " + t.toString());
					}

					/**Threshold sim*/
					if (similarity >= 0.6){
						localGoodTriples.add(t);
						goodTriples.add(t);
					}
					//System.out.println(similarity + " " + t.toString());
				}		
			}
		}
		if (goodTriples.isEmpty() && !bestTriple.isNullTriple()){
			goodTriples.add(bestTriple);
		}
		/*if (bestTriple !=null){
			goodTriples.add(bestTriple);
		}*/
		/*for (String s: goodTriples){
			System.out.println(s);
		}*/
		//System.out.println(maxSim +" "+ bestTriple.toString());
		// 	- all entities' type in the sub graph
		// 	- Zheng: sim(concat(neighbors+topconcept), semantic meaning): too restrictive ?
		return goodTriples;
	}

	public String toString(){
		return this.res.toValueString();
	}

	public int hashCode(){
		return this.res.toValueString().hashCode();
	}

	public Resource getRes(){
		return this.res;
	}

	public boolean equals(Object obj){
		if (obj instanceof SingleAnswer){
			return this.res.toValueString().equals(((SingleAnswer)obj).res.toValueString());
		}
		else{
			return false;
		}
	}
	
	public boolean hasMatch(){ 
		return !this.res.getSimilarIRIs().isEmpty();
	}
	
	public String printMatchedEquivalents() {
		return this.res.getSimilarIRIs().toString();
	}

}
