package fr.irit.complex;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import oaeicompare.OAEICompare;

import org.apache.jena.base.Sys;
import org.semanticweb.owl.align.AlignmentException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;

import fr.irit.complex.answer.Answer;
import fr.irit.complex.answer.PairAnswer;
import fr.irit.complex.answer.SingleAnswer;
import fr.irit.complex.subgraphs.TripleSubgraph;
import fr.irit.complex.subgraphs.InstantiatedSubgraph;
import fr.irit.complex.subgraphs.Path;
import fr.irit.complex.subgraphs.PathSubgraph;
import fr.irit.complex.subgraphs.SubgraphForOutput;
import fr.irit.complex.subgraphs.Triple;
import fr.irit.complex.utils.Utils;
import fr.irit.resource.IRI;
import fr.irit.resource.Resource;
import fr.irit.sparql.Proxy.SparqlProxy;
import fr.irit.sparql.exceptions.NotAFolderException;
import fr.irit.sparql.query.Exceptions.SparqlEndpointUnreachableException;
import fr.irit.sparql.query.Exceptions.SparqlQueryMalFormedException;
import fr.irit.sparql.query.Select.SparqlSelect;

public class ComplexAlignmentGeneration {

	/**
	 * @param args
	 */

	//A mettre dans pom.xml sous mvn
	/*static {
		System.setProperty("log4j.configuration", "log4j.properties");
		System.setProperty("log4j.configurationFile", "log4j2.xml");
		PropertyConfigurator.configure(System.getProperty("log4j.configuration"));
	}*/

	public static void main(String[] args) {

		System.out.println(Instant.now());


		long tm = System.nanoTime();
		/** Input verification**/
		if(args.length < 1){
			System.err.println("At least 1 argument is expected, you provided: " +args.length+
					"\nPlease refer to Readme.md file");
		}
		else{
			try {
				/**Read the parameter file*/
				System.out.println("===============================================================================");
				Utils utils = Utils.getInstance();
				boolean systemCanRun = utils.init(args[0]);
				String sourceEndpoint = utils.getSourceEndpoint();
				String targetEndpoint = utils.getTargetEndpoint();
				int maxMatches = 10;
				double similarityThreshold = 0.6;
				boolean reassess = false;
				if(args.length == 3) {
					maxMatches = Integer.parseInt(args[1]);
					similarityThreshold = Double.parseDouble(args[2]);
				}
				else if(args.length == 4) {
					maxMatches = Integer.parseInt(args[1]);
					similarityThreshold = Double.parseDouble(args[2]);
					if(args[3].equals("reassess")) {
						reassess = true;
					}
				}
				System.out.println("Running with "+maxMatches+" support instances - "+similarityThreshold+" similarity.");

				if(systemCanRun){
					/** For each query in query repository**/
					System.out.println("Number of CQAs: "+utils.getQueries().size());
					for (SparqlSelect sq : utils.getQueries()){
						System.out.println();
						if(sq.getFocusLength() == 1){
							System.out.println("Unary query : " + sq.toSubgraphForm());
						}
						else if (sq.getFocusLength() == 2){	 
							System.out.println("Binary query : " + sq.toSubgraphForm());
						}
						//SparqlSelect sq = new SparqlSelect (query);

						// Retrieve query's URI's labels 
						HashMap<String, IRI> iriList = sq.getIRIList();
						for (Map.Entry<String, IRI> m: iriList.entrySet()){
							m.getValue().retrieveLabels(sourceEndpoint);
							//m.getValue().printLabels();
						}            

						//- Retrieve list of query's entities/pairs
						//System.out.println("Getting source answers ...");
						ArrayList<Answer> answers = new ArrayList<Answer>();
						HashSet<Answer> matchedAnswers = new HashSet<Answer>();
						int offsetMatch = 0;

						boolean noMoreSourceAnswers = false;
						int offset = 0;
						int limit =2000;
						while (!noMoreSourceAnswers && matchedAnswers.size() < maxMatches){
							String queryLimit = " LIMIT "+limit+"\n OFFSET "+offset;

							//Unary queries
							if(sq.getFocusLength() == 1){
								SparqlProxy spIn = SparqlProxy.getSparqlProxy(sourceEndpoint);
								ArrayList<JsonNode> ret = spIn.getResponse(sq.toUnchangedString() + queryLimit);
								Iterator<JsonNode> retIterator = ret.iterator();
								while (retIterator.hasNext()) {
									JsonNode response = retIterator.next();
									String s = response.get(sq.getSelectFocus().get(0).replaceFirst("\\?", "")).get("value").toString().replaceAll("\"", "");
									String type = response.get(sq.getSelectFocus().get(0).replaceFirst("\\?", "")).get("type").toString().replaceAll("\"", "");
									if(!type.equals("bnode")){
										SingleAnswer singleton= new SingleAnswer(new Resource(s));
										answers.add(singleton);	   
									}

								}
								if(ret.size() < limit){
									noMoreSourceAnswers = true;
								}							
								// System.out.println(answers.get(0).answerType());
							}

							//Binary queries
							else if (sq.getFocusLength() == 2){	 
								SparqlProxy spIn = SparqlProxy.getSparqlProxy(sourceEndpoint);
								ArrayList<JsonNode> ret = spIn.getResponse(sq.toUnchangedString() + queryLimit);
								Iterator<JsonNode> retIterator = ret.iterator();
								while (retIterator.hasNext()) {	
									JsonNode response = retIterator.next();
									String s1 = response.get(sq.getSelectFocus().get(0).replaceFirst("\\?", "")).get("value").toString().replaceAll("\"", "");
									String s2 = response.get(sq.getSelectFocus().get(1).replaceFirst("\\?", "")).get("value").toString().replaceAll("\"", "");
									String type1 = response.get(sq.getSelectFocus().get(0).replaceFirst("\\?", "")).get("type").toString().replaceAll("\"", "");
									String type2 = response.get(sq.getSelectFocus().get(1).replaceFirst("\\?", "")).get("type").toString().replaceAll("\"", "");
									if(!type1.equals("bnode") && !type2.equals("bnode")){ 
										if(!s1.equals("") && !s2.equals("")) {
											PairAnswer pair = new PairAnswer(new Resource(s1), new Resource(s2));
											answers.add(pair);  
										}	
									}
								}
								if(ret.size() < limit){
									noMoreSourceAnswers = true;
								}
							}
							else{
								System.out.println("ERROR for query : " +sq.toUnchangedString());
								System.err.println("Problem detected: too many variables in SELECT: can only deal with 1 or 2");
								noMoreSourceAnswers=true;
							}


							if(!noMoreSourceAnswers){
								offset+=limit;
							}


							while(matchedAnswers.size() < maxMatches && offsetMatch < answers.size()) {

								Answer ans = answers.get(offsetMatch);
								ans.getExistingMatches(sourceEndpoint, targetEndpoint);
								if (ans.hasMatch()){
									matchedAnswers.add(ans);
									//	System.out.println(ans);
//									System.out.println(ans.printMatchedEquivalents());
								}
								offsetMatch ++;
							}
						}
						System.out.println("Number of matched answers :" +matchedAnswers.size());

						//if final set of answers is empty (ie if not existing match was found)

						//Look for existing matches						
						Iterator<Answer> ansIt = answers.iterator();
						if (matchedAnswers.isEmpty()){
							System.out.println("Looking for similar answers");
							System.out.println("Answer size: " + answers.size());
							ansIt = answers.iterator();
							// while nb of answers with similarIRIs <thrshold && not end of answer list
							while (matchedAnswers.size() < maxMatches && ansIt.hasNext()){
								Answer ans = ansIt.next();
								ans.retrieveIRILabels(sourceEndpoint);
								ans.getSimilarIRIs(targetEndpoint,sourceEndpoint);
								if (ans.hasMatch()){
									//System.out.println(ans);
//									System.out.println(ans.printMatchedEquivalents());
									matchedAnswers.add(ans);
								}
							}

							System.out.println("Number of similar answers :" +matchedAnswers.size());
						}
//						System.out.println(matchedAnswers);
						HashSet<InstantiatedSubgraph> goodSubgraphs = new HashSet<InstantiatedSubgraph>();


						// Find corresponding subgraph of matched answers
						for (Answer ans: matchedAnswers){
							HashSet <InstantiatedSubgraph> localSubgraphs = ans.findCorrespondingSubGraph(sq,targetEndpoint,similarityThreshold,sourceEndpoint);
							for (InstantiatedSubgraph t : localSubgraphs){
								goodSubgraphs.add(t);
							}
						}
//						System.out.println(goodSubgraphs);
						//Code above replaces this one
						/*for (Answer ans: answers){
							ans.retrieveIRILabels(sourceEndpoint);
							//System.out.println(ans.toString());
							ans.getExistingMatches(sourceEndpoint, targetEndpoint);
							ans.getSimilarIRIs(targetEndpoint);
							HashSet <Triple> localTriples = ans.findCorrespondingSubGraph(sq,targetEndpoint);
							for (Triple t : localTriples){
								goodTriples.add(t);
							}
						}*/

						/**Find common things in triples
						 * Add them in goodTriples*/
						//System.out.println("Sorting triples ...");

						ArrayList<SubgraphForOutput> output = new ArrayList<SubgraphForOutput>();

						for (InstantiatedSubgraph t: goodSubgraphs){
							boolean added = false;
							Iterator<SubgraphForOutput> it = output.iterator();
							while (it.hasNext() && !added){
								SubgraphForOutput subG = it.next();
								if(t instanceof Triple && subG instanceof TripleSubgraph) {
									added = ((TripleSubgraph) subG).addSubgraph((Triple)t);
								}
								if(t instanceof Path  && subG instanceof PathSubgraph) {
									added = ((PathSubgraph)subG).addSubgraph((Path)t);
								}																
							}
							if (!added){
								if(t instanceof Triple) {
									output.add(new TripleSubgraph((Triple)t));
								}
								if(t instanceof Path) {
									output.add(new PathSubgraph((Path)t));
								}	
							}
						}

						System.out.println("Number of correspondences found: "+output.size());
						//System.out.println(output.get(0).toExtensionString());
						//	reassess confidence based on matched counter examples

						if(reassess) {
							System.out.println("Reassessing similarity");
							for(SubgraphForOutput s: output) {
								System.out.println(s);
								//								System.out.println(((TripleSubgraph)s).predicateHasMaxSim());
								s.reassessSimilarityWithCounterExamples(sourceEndpoint, targetEndpoint, sq);
								//								System.out.println(s);
							}
						}

						Collections.sort(output);


						ArrayList<SubgraphForOutput> singleOutput = new ArrayList<SubgraphForOutput>();
						if(output.size()>0 && output.get(output.size()-1).getSimilarity()<0.6 && output.get(output.size()-1).getSimilarity()>0.01) {
							double sim = output.get(output.size()-1).getSimilarity();
							boolean moreCorrespondences= true;
							int i = output.size()-1;
							while( i >=0 && moreCorrespondences) {
								if(output.get(i).getSimilarity() == sim) {
									singleOutput.add(output.get(i));
									System.out.println(output.get(i));
								}
								else {
									moreCorrespondences = false;
								}
								i--;
							}
						}
						else {
							for(SubgraphForOutput s: output) {
								if(s.getSimilarity() >= 0.6) {
									singleOutput.add(s);
								}								
								System.out.println(s);
							}
						}

						//System.out.println("Adding correspondences to output");
						if(!singleOutput.isEmpty()) {
							utils.addToOutput(sq, singleOutput);
						}						
					}
				}

				//Mandatory to "end" the process (print alignments, etc.)
				utils.end();
				System.out.println("Matching process ended");


			}catch (SparqlQueryMalFormedException | SparqlEndpointUnreachableException | FileNotFoundException ex) {
				Logger.getLogger(OAEICompare.class.getName()).log(Level.SEVERE, null, ex);
			} catch (JsonProcessingException e1) {
				e1.printStackTrace();
			} catch (IOException e1) {
				e1.printStackTrace();
			} catch (AlignmentException e) {
				e.printStackTrace();
			} catch (URISyntaxException e) {
				e.printStackTrace();
			} catch (NotAFolderException e) {
				e.printStackTrace();
			}

		}


		System.out.println("Run time: " + (System.nanoTime() - tm) / 10e8);
		System.out.println(Instant.now());
	}
}

