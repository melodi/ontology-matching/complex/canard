package fr.irit.complex.subgraphs;

import java.util.ArrayList;
import java.util.Iterator;

import com.fasterxml.jackson.databind.JsonNode;

import fr.irit.resource.IRI;
import fr.irit.resource.Resource;
import fr.irit.sparql.Proxy.SparqlProxy;
import fr.irit.sparql.query.Exceptions.SparqlEndpointUnreachableException;
import fr.irit.sparql.query.Exceptions.SparqlQueryMalFormedException;
import fr.irit.sparql.query.Select.SparqlSelect;

public class SubgraphForOutput implements Comparable<SubgraphForOutput>{
	double similarity;

	public String toExtensionString() {return "";}

	public String toIntensionString() {return "";}

	public String toString() {
		return this.getAverageSimilarity()+" <-> "+this.toIntensionString();
	}
	public double getSimilarity() {
		return this.similarity;
	}
	public double getAverageSimilarity() {return this.similarity;}

	public boolean addSubgraph(InstantiatedSubgraph s) {
		return true;
	}

	public void reassessSimilarityWithCounterExamples(String sourceEndpoint, String targetEndpoint, SparqlSelect sq) {
		//For each result of subGraphToSPARQLForm on targetEndpoint
		SparqlProxy spTarg = SparqlProxy.getSparqlProxy(targetEndpoint);
		SparqlProxy spSource = SparqlProxy.getSparqlProxy(sourceEndpoint);
		double nbTrueExamples =0;
		double nbCounterExamples = 0;
		double nbRetSource = 0;
		try {
			ArrayList<JsonNode> retSource= spSource.getResponse(sq);
			nbRetSource = retSource.size();
			//			System.out.println(this.toSPARQLForm());
			int offset = 0;
			int limit = 10000;
			boolean end = false;
			while (!end) {
				String newQuery = this.toSPARQLForm();
				newQuery += "\n LIMIT "+limit;
				newQuery += "\n OFFSET "+offset;
				ArrayList<JsonNode> ret= spTarg.getResponse(newQuery);
				Iterator<JsonNode> retIterator = ret.iterator();
//				System.out.println(newQuery);
				while (retIterator.hasNext() && nbCounterExamples <= 10*nbRetSource) {
//					System.out.println(nbCounterExamples+" "+nbTrueExamples+" "+nbRetSource);
					JsonNode response = retIterator.next();
					//Find equivalent IRIs in source
					if(response.has("answer")) {
						IRI iriResponse = new IRI("<"+response.get("answer").get("value").toString().replaceAll("\"", "")+">");
						//iriResponse.retrieveLabels(targetEndpoint);
						//iriResponse.findSimilarResource(sourceEndpoint);
						iriResponse.findExistingMatches(targetEndpoint, sourceEndpoint);
						// ASK equivalent IRI with Query on sourceEndpoint
						for(IRI sourceRes: iriResponse.getSimilarIRIs()) {
							if(spSource.sendAskQuery("ASK{"+sq.toSubgraphForm().replaceAll("\\?answer", sourceRes.toString())+"}")) {
								nbTrueExamples+=1;
							}
							else {
								nbCounterExamples+=1;
							}
						}					

					}
					if(response.has("answer1")) {
						Resource r1 = new Resource(response.get("answer0").get("value").toString().replaceAll("\"", ""));
						Resource r2 = new Resource(response.get("answer1").get("value").toString().replaceAll("\"", ""));
						ArrayList<Resource> valuesr1Source = new ArrayList<Resource>();
						ArrayList<Resource> valuesr2Source = new ArrayList<Resource>();

						if(r1.isIRI()){
							r1 = new IRI("<"+r1.toString()+">");
							((IRI)r1).findExistingMatches(targetEndpoint, sourceEndpoint);
							for(IRI i : r1.getSimilarIRIs()) {
								valuesr1Source.add(i);
							}
						}
						else {
							valuesr1Source.add(r1);
						}
						if(r2.isIRI()){
							r2 = new IRI("<"+r2.toString()+">");
							((IRI)r2).findExistingMatches(targetEndpoint, sourceEndpoint);
							for(IRI i : r2.getSimilarIRIs()) {
								valuesr2Source.add(i);
							}
						}
						else {
							valuesr2Source.add(r2);
						}
						// ASK equivalent IRI with Query on sourceEndpoint
						for(Resource sourceRes1: valuesr1Source) {
							for(Resource sourceRes2: valuesr2Source) {
								String query = sq.toSubgraphForm();
								if(sourceRes1.isIRI()) {
									query=query.replaceAll("\\?answer0", sourceRes1.toString());
								}else {
									query+=" Filter(str(?answer0)="+sourceRes1.toValueString()+")";
								}
								if(sourceRes2.isIRI()) {
									query=query.replaceAll("\\?answer1", sourceRes2.toString());
								}else {
									query+=" Filter(str(?answer1)="+sourceRes2.toValueString()+")";
								}
								query="ASK{"+query+"}";
								if(spSource.sendAskQuery(query)) {
									nbTrueExamples+=1;
								}
								else {
									nbCounterExamples+=1;
								}
							}
						}					
					}
				}// get target answers
				if(ret.size() <limit) {
					end = true;					
				}
				else {
					offset+=limit;
				}
				if(nbCounterExamples >= 10*nbRetSource) {
					end = true;
				}
				if (offset > 600000) {
					end = true;
				}
			}// while (!end)

		} catch (SparqlQueryMalFormedException | SparqlEndpointUnreachableException e) {
			e.printStackTrace();
		}

		//		System.out.println(nbTrueExamples+" "+nbCounterExamples);

		if(nbTrueExamples+nbCounterExamples == 0) {
			this.similarity = 0;
		}else {
			double percentageCommonOK = nbTrueExamples/(nbTrueExamples+nbCounterExamples);
			this.similarity *= percentageCommonOK;
		}

	}

	public String toSPARQLForm() {
		return "";
	}

	@Override
	public int compareTo(SubgraphForOutput o) {
		return Double.compare(this.getSimilarity(), o.getSimilarity());
	}

}
