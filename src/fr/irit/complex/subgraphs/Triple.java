package fr.irit.complex.subgraphs;

import java.util.HashSet;

import fr.irit.complex.utils.Utils;
import fr.irit.resource.IRI;
import fr.irit.resource.Resource;
import fr.irit.sparql.query.Exceptions.SparqlEndpointUnreachableException;
import fr.irit.sparql.query.Exceptions.SparqlQueryMalFormedException;

public class Triple extends InstantiatedSubgraph {
	private IRI subject;
	private IRI predicate;
	private Resource object;
	/**TYPE : 1 for subject, 2 for predicate, 3 for object*/
	private int type; 
	private boolean visited ; 
	public boolean keepObjectType;
	public boolean keepSubjectType;
	private IRI subjectType;
	private IRI objectType;
	private double objectSimilarity;
	private double subjectSimilarity;
	private double predicateSimilarity;

	public Triple(){
		this.subject = new IRI("");
		this.object = new Resource("");
		this.predicate = new IRI("");
	}

	public Triple(String sub, String pred, String obj, int type){
		this.subject = new IRI(sub);
		this.predicate = new IRI(pred);
		Resource r = new Resource(obj);
		if(r.isIRI()){
			this.object = new IRI("<"+obj.replaceAll("[<>]", "")+">");
		}
		else{
			this.object = r;
		}
		this.type = type;
		this.visited = false;
		this.keepObjectType = false;
		this.keepSubjectType = false;
		this.objectSimilarity = 0;
		this.subjectSimilarity = 0;
		this.predicateSimilarity = 0;
	}

	public void retrieveIRILabels(String targetEndpoint) throws SparqlQueryMalFormedException, SparqlEndpointUnreachableException{
		if (type!= 1){
			subject.retrieveLabels(targetEndpoint);
		}
		if(type!=2){
			predicate.retrieveLabels(targetEndpoint);
		}
		if(type !=3 && object instanceof IRI){
			((IRI)object).retrieveLabels(targetEndpoint);
		}
	}

	public void retrieveTypes(String targetEndpoint) throws SparqlQueryMalFormedException, SparqlEndpointUnreachableException{
		if (type!= 1){
			subject.retrieveTypes(targetEndpoint);
		}
		if(type!=2){
			predicate.retrieveTypes(targetEndpoint);
		}
		if(type !=3 && object instanceof IRI){
			((IRI)object).retrieveTypes(targetEndpoint);
		}		
	}


	/**Compares labels and type labels*/
	public double compareLabel(HashSet<String> targetLabels, double threshold, String targetEndpoint){
		if (type !=1){
			subjectType = subject.findMostSimilarType(targetEndpoint, targetLabels, threshold);
			double scoreTypeSubMax = 0;
			if(subjectType !=null) {
				scoreTypeSubMax = Utils.similarity(subjectType.getLabels(), targetLabels, threshold);
			}
			this.subjectSimilarity = Utils.similarity(subject.getLabels(), targetLabels, threshold);
			if (scoreTypeSubMax > this.subjectSimilarity){
				keepSubjectType = true;
				this.subjectSimilarity = scoreTypeSubMax;
			}
		}
		if (type !=2 && !this.predicate.toString().equals("<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>")){
			this.predicateSimilarity = Utils.similarity(predicate.getLabels(), targetLabels, threshold);
		}
		if (type !=3 && object instanceof IRI){
			objectType = ((IRI)object).findMostSimilarType(targetEndpoint, targetLabels, threshold);
			//System.out.println(objectType);
			if(objectType !=null) {
				double scoreTypeObMax = Utils.similarity(objectType.getLabels(), targetLabels, threshold);
				this.objectSimilarity = Utils.similarity(((IRI)object).getLabels(), targetLabels, threshold);
				if (scoreTypeObMax > this.objectSimilarity){
					keepObjectType = true;
					this.objectSimilarity = scoreTypeObMax;
				}
			}
			
		}
		else if (type !=3){
			HashSet<String> hashObj = new HashSet<String>();
			hashObj.add(object.toString());
			this.objectSimilarity= Utils.similarity(hashObj, targetLabels, threshold);
		}
		return this.subjectSimilarity + this.predicateSimilarity + this.objectSimilarity;

	}

	public boolean objectIsIRI(){
		return this.object instanceof IRI;
	}

	public IRI getSubject() {
		return subject;
	}
	public void setSubject(IRI subject) {
		this.subject = subject;
	}
	public IRI getPredicate() {
		return predicate;
	}
	public void setPredicate(IRI predicate) {
		this.predicate = predicate;
	}
	public Resource getObject() {
		return object;
	}
	public void setObject(Resource object) {
		this.object = object;
	}

	public boolean isSubjectTriple(){
		return this.type == 1;
	}

	public boolean isPredicateTriple(){
		return this.type == 2;
	}

	public boolean isObjectTriple(){
		return this.type == 3;
	}

	public IRI getObjectType(){
		return this.objectType;
	}

	public IRI getSubjectType(){
		return this.subjectType;
	}

	public String toString(){
		String subjStr = subject.toValueString();
		String predStr = predicate.toValueString();
		String objStr = object.toValueString();

		if (this.isSubjectTriple()){
			subjStr = "?answer";
		}
		else if (this.isPredicateTriple()){
			predStr = "?answer";
		}
		else if (this.isObjectTriple()){
			objStr = "?answer";
		}

		String result = subjStr + " " + predStr + " " + objStr + ". ";
		if (keepSubjectType && !keepObjectType){
			result = "?x " + predStr + " " + objStr + ". " +
					"?x a "+subjectType + ". ";
		}
		else if (keepObjectType && !keepSubjectType){
			result = subjStr + " " + predStr + " ?y. " +
					"?y a "+objectType + ". ";
		}
		else if (keepObjectType && keepSubjectType){
			result = "?x " + predStr + " ?y. " +
					"?y a "+objectType + ". " +
					"?x a "+subjectType + ". ";
		}
		return result;
	}


	public int commonPartValue(Triple t){
		int res = -1;
		if (this.getType() == t.getType()){
			if (this.getPredicate().equals(t.getPredicate()) && !this.isPredicateTriple()){
				res = 2;
			}
			if (this.getObject().equals(t.getObject()) && !this.isObjectTriple() && !this.keepObjectType){
				res = 3;
			}
			if (this.getSubject().equals(t.getSubject()) && !this.isSubjectTriple() && ! this.keepSubjectType){
				res = 1;
			}
		}
		return res;
	}

	/*	public String getCommonPart(Triple t){
		String res = this.toString();
		if(this.getType() == t.getType()){
			if (this.isSubjectTriple()){
				if (this.getPredicate().equals(t.getPredicate())){
					res = res.replaceFirst(this.object.toString(), "?someObject");
				}
//				else if (this.getObject().equals(t.getObject())){
//					res = res.replaceFirst(this.predicate.toString(), "?somePredicate");
//				}
			}
			else if (this.isPredicateTriple()){
				if (this.getSubject().equals(t.getSubject())){
					res = res.replaceFirst(this.object.toString(), "?someObject");
				}
				else if (this.getObject().equals(t.getObject())){
					res = res.replaceFirst(this.subject.toString(), "?someSubject");
				}
			}
			else if (this.isObjectTriple()){
//				if (this.getSubject().equals(t.getSubject())){
//					res = res.replaceFirst(this.predicate.toString(), "?somePredicate");
//				}
//				else
				 if (this.getPredicate().equals(t.getPredicate())){
					res = res.replaceFirst(this.subject.toString(), "?someSubject");
				}
			}

		}
		return res;
	}*/

	public boolean hasCommonPart (Triple t){
		boolean res = false;
		if (this.getType() == t.getType()){
			if(! this.isSubjectTriple()){
				res = res || this.getSubject().equals(t.getSubject());
	//			res = res || (this.keepSubjectType && t.keepSubjectType && this.subjectType.equals(t.subjectType));
			}
			if(! this.isPredicateTriple()){
				res = res || this.getPredicate().equals(t.getPredicate());
			}
			if(! this.isObjectTriple()){
				res = res || this.getObject().equals(t.getObject());
	//			res = res || (this.keepObjectType && t.keepObjectType && this.objectType.equals(t.objectType));
			}
		}
		return res;
	}

	public void printLabels(){
		System.out.println("Subject Labels: ");
		subject.printLabels();
		System.out.println("Predicate Labels: ");
		predicate.printLabels();
		System.out.println("Object Labels: ");
		if (object instanceof IRI){
			((IRI)object).printLabels();
		}
		else{
			System.out.println(object.toString());
		}
	}


	public boolean samePredicateAs(Triple t){
		return this.predicate.toString().equals(t.predicate.toString());
	}

	public boolean sameSubjectAs(Triple t){
		return this.subject.toString().equals(t.subject.toString());
	}

	public boolean sameObjectAs(Triple t){
		return this.object.toString().equals(t.object.toString());
	}

	public int getType(){
		return this.type;
	}

	/*public String toSPARQLTriple(){
		if(this.isSubjectTriple()){
			return "?answer "+this.getPredicate().toString() + " " + this.getObject().toString();
		}
		else if(this.isPredicateTriple()){
			return this.getSubject().toString() + " ?answer " + this.getObject().toString();

		}else if(this.isObjectTriple()){
			return this.getSubject()+" "+this.getPredicate().toString() + " ?answer";
		}
		else {
			return this.toString();
		}
	}*/

	public boolean isVisited(){
		return this.visited;
	}

	public void setVisited(boolean b){
		this.visited = b;
	}

	public int hashCode(){
		return (subject.toString() + predicate.toString() +object.toString()).hashCode();
	}

	public boolean equals (Object obj){
		if (obj instanceof Triple){
			//return this.toString().equals(((Triple)obj).toString());
			return (subject.toString() + predicate.toString() +object.toString())
					.equals(((Triple)obj).subject.toString() + ((Triple)obj).predicate.toString() +((Triple)obj).object.toString());
		}
		else{
			return false;
		}

	}

	public boolean isNullTriple(){
		return this.subject.toString().equals("") && this.predicate.toString().equals("") && this.object.toString().equals("");
	}


	public double getSimilarity(){
		return this.subjectSimilarity + this.predicateSimilarity + this.objectSimilarity;
	}

	public Resource getAnswer(){
		switch (type){
		case 1: // subject
			return this.subject;
		case 2: //predicate
			return this.predicate;
		case 3: //object
			return this.object;
		default:
			return new Resource("null");
		}
	}

	public double getObjectSimilarity(){
		return this.objectSimilarity;
	}
	public double getSubjectSimilarity(){
		return this.subjectSimilarity;
	}
	public double getPredicateSimilarity(){
		return this.predicateSimilarity;
	}
	
	public int getPartGivingMaxSimilarity() {
		int res = 0;
		if (this.subjectSimilarity > this.objectSimilarity && this.subjectSimilarity > this.predicateSimilarity) {
			res =1;
		}
		else if	(this.objectSimilarity > this.subjectSimilarity && this.objectSimilarity > this.predicateSimilarity) {
			res =3;
		}
		else if (this.predicateSimilarity > this.subjectSimilarity && this.predicateSimilarity > this.objectSimilarity) {
			res =2;
		}
		return res;
		
	}
}
