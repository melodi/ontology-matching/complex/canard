package fr.irit.complex.subgraphs;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;

public class TripleSubgraph extends SubgraphForOutput {

	ArrayList <Triple> triples;
	int commonPart; 	/** commonPart : 1-subject 2-predicate 3-object  */
	double maxSimilarity;
	boolean formsCalculated;
	String intension;
	String extension;
	int partWithMaxSim;


	public TripleSubgraph(Triple t){
		triples = new ArrayList <Triple> ();
		triples.add(t);
		commonPart = -1;
		maxSimilarity = t.getSimilarity();
		similarity = t.getSimilarity();
		this.formsCalculated = false;
		partWithMaxSim=t.getPartGivingMaxSimilarity();
	}

	public boolean addSubgraph(Triple t){
		boolean added=false;
		if(triples.get(0).toString().equals(t.toString())) {
			triples.add(t);
			added=true;
		}
		else if (triples.get(0).hasCommonPart(t) && commonPart == -1){
			if(!triples.get(0).getPredicate().toString().equals("<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>")|| triples.get(0).commonPartValue(t) !=2) {
				addSimilarity(t);
				triples.add(t);
				commonPart = triples.get(0).commonPartValue(t);			
				added=true;
			}

		}
		else if (triples.get(0).hasCommonPart(t) && triples.get(0).commonPartValue(t) == commonPart ){
			if(!triples.get(0).getPredicate().toString().equals("<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>")|| commonPart !=2) {
				addSimilarity(t);
				triples.add(t);
				added=true;
//				System.out.println(triples.get(0)+"  "+t+"commonPart=" + commonPart );
			}			
		}
		/*if(t.getPartGivingMaxSimilarity() != partWithMaxSim) {
			System.out.println(t.toString()+" does not have same part with max sim");
		}*/
		return added;
	}

	public void printAnswers(){
		for (Triple t: triples){
			System.out.print(t.getAnswer()+" ");
		}
		System.out.println();
	}

	public void addSimilarity(Triple t){
		maxSimilarity = Math.max(maxSimilarity, t.getSimilarity());
		similarity = ((similarity*triples.size())+t.getSimilarity())/(triples.size()+1);
	}

	public String calculateIntensionString(){
		String res = triples.get(0).toString();
		Triple t = triples.get(0);
		HashSet<String> concatSub = new HashSet<>();
		HashSet<String> concatPred = new HashSet<>();
		HashSet<String> concatObj = new HashSet<>();
		for (Triple t1 : triples){
			concatSub.add(t1.getSubject().toString());
			concatPred.add(t1.getPredicate().toString());
			concatObj.add(t1.getObject().toString());
		}
		if (t.isSubjectTriple() && !t.getPredicate().toString().equals("<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>")){
			if (commonPart == 2 && concatObj.size() > 1){
				res = res.replaceFirst(stringToRegex(t.getObject().toValueString()), "?someObject");
			}
			else if (commonPart == 3 && concatPred.size() > 1){
				res = res.replaceFirst(t.getPredicate().toValueString(), "?somePredicate");
			}
			else if (commonPart==-1 && predicateHasMaxSim()) {
				res = res.replaceFirst(stringToRegex(t.getObject().toValueString()), "?someObject");
			}
		}
		else if (t.isPredicateTriple()){
			if (commonPart == 1 && concatObj.size() > 1){
				res = res.replaceFirst(stringToRegex(t.getObject().toValueString()), "?someObject");
			}
			else if (commonPart == 3 && concatSub.size() > 1){
				res = res.replaceFirst(t.getSubject().toValueString(), "?someSubject");
			}
		}
		else if (t.isObjectTriple() && !t.getPredicate().toString().equals("<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>")){
			if (commonPart == 1 && concatPred.size() > 1){
				res = res.replaceFirst(t.getPredicate().toValueString(), "?somePredicate");
			}
			else if (commonPart == 2 && concatSub.size() > 1){
				res = res.replaceFirst(t.getSubject().toValueString(), "?someSubject");
			}
			else if (commonPart==-1 && predicateHasMaxSim()) {
				res = res.replaceFirst(t.getSubject().toValueString(), "?someSubject");
			}
		}

		//return this.maxSimilarity+" -- "+res;
		this.intension = res;
		return res;
	}

	public String calculateExtensionString(){
		String res = this.intension;
		HashSet<String> concatSub = new HashSet<>();
		HashSet<String> concatPred = new HashSet<>();
		HashSet<String> concatObj = new HashSet<>();
		for (Triple t : triples){
			concatSub.add(t.getSubject().toString());
			concatPred.add(t.getPredicate().toString());
			concatObj.add(t.getObject().toString());
		}


		res = res.replaceAll("\\?someSubject", concatSub.toString());

		res = res.replaceAll("\\?somePredicate", concatPred.toString());

		res = res.replaceAll("\\?someObject", concatObj.toString());

		res = res.replaceAll("\\[", "\\{").replaceAll("\\]", "\\}");
		this.extension = res;

		return res;
	}

	//	public String calculateExtensionString() {
	//		String res = "";
	//		if(calculateExtensionStringOld().equals(this.intension)) {
	//			res=this.intension;
	//		}
	//		else {
	//			for(Triple t: triples) {
	//				res+="UNION{"+t.toString()+"}";
	//			}
	//			res=res.replaceFirst("UNION", "");
	//		}
	//
	//		this.extension=res;
	//		return res;
	//	}

	public String toIntensionString(){
		if(!this.formsCalculated){
			this.calculateIntensionString();
			this.calculateExtensionString();
			this.formsCalculated = true;
		}
		return this.intension;
	}

	public String toExtensionString(){
		if(!this.formsCalculated){
			this.calculateIntensionString();
			this.calculateExtensionString();
			this.formsCalculated = true;
		}
		return this.extension;
	}
	
	public String toSPARQLForm() {
		String res ="SELECT DISTINCT ?answer WHERE {";
		if (this.toIntensionString().contains("somePredicate")){ 
			res+=this.toSPARQLExtension();
		}
		// If common part is the predicate 
		else if (this.commonPart == 2 || this.commonPart == -1){
			// and if the predicate similarity is higher than the object/subject similarity --> Intension
			if(this.predicateHasMaxSim()) {
				res+=this.intension;
			}					
			// else --> extension
			else {
				res+= this.toSPARQLExtension();
			}		

		}
		else {
			res+= this.toSPARQLExtension();
		}
		
		res +="}";
		return res;
	}
	
	public String toSPARQLExtension() {
		HashSet<String> concatTriple = new HashSet<>();
		for (Triple t1 : triples){
			concatTriple.add(t1.toString());
		}
		ArrayList<String> unionMembers = new ArrayList<String>();
		for(String strTriple: concatTriple) {
			unionMembers.add(strTriple);
		}
		String res = "";
		
		if(this.toIntensionString().equals(this.extension)) {
			res = this.extension;
		}
		else if (unionMembers.size() > 1){
			res+="{"+unionMembers.get(0).toString()+"}\n";
			for(int i =1 ; i < unionMembers.size();i++) {
				res+="UNION {"+unionMembers.get(i).toString()+"}\n";
			}			
		}
		return res;
	}

	public void sortTriples(){
		Collections.sort(triples);
	}

	public ArrayList<Triple> getTriples(){
		return this.triples;
	}

	public double getAverageSimilarity(){
		return similarity;
	}

	public boolean predicateHasMaxSim() {
		return this.partWithMaxSim == 2;
	}
	
	private String stringToRegex(String s) {
		s= s.replaceAll("\\{", "\\\\{");
		s = s.replaceAll("\\}", "\\\\}");
		s=s.replaceAll("\\[", "\\\\[");
		s=s.replaceAll("\\]", "\\\\]");
		s=s.replaceAll("\\.", "\\\\.");
		s=s.replaceAll("\\?", "\\\\?");
		s=s.replaceAll("\\+", "\\\\+");
		s=s.replaceAll("\\*", "\\\\*");
		s=s.replaceAll("\\|", "\\\\|");
		s=s.replaceAll("\\^", "\\\\^");
		s=s.replaceAll("\\$", "\\\\$");
		return s;
	}


}